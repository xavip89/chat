var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var dateFormat = require('dateformat');

var format = 'hh:MM:ss';

app.use(express.static(__dirname + '/js'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var allSockets = [];

io.on('connection', function(socket) {
	var userId = allSockets.push(socket);

	socket.on('message', function(msg){
		io.sockets.emit('message', {date: dateFormat(msg.date, format), text: msg.text, user: msg.user});
	});

	socket.on('disconnect', function(msg){
		io.sockets.emit('disconnected', {date: dateFormat(new Date(), format), text: "User disconnected :(", user: msg.user});
	});

	socket.emit("identify", {user: userId});
	io.sockets.emit('connected', {date: dateFormat(new Date(), format), text: "New user connected :)", user: userId});

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});