var users = {};
var dim = 4;

function newComment(msg) {
	if (!users[msg.user]) users[msg.user] = newAvatar("avatar_" + msg.user, "svg", dim);
	var s = '<div class="row img-rounded comment">';
	s += '<div class="col-xs-1 col-md-1 col-sm-1 col-lg-1"><div class="avatar">' + users[msg.user].outerHTML + '</div></div>';
	s += '<div class="col-xs-9 col-md-9 col-sm-9 col-lg-9">' + msg.text + '</div>';
	s += '<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2">' + msg.date + '</div>';
	return s;
}

function newConnected(msg) {
	users[msg.user] = newAvatar("avatar_" + msg.user, "svg", dim);
	var s = '<div class="row img-rounded connected bg-primary">';
	s += '<div class="col-xs-1 col-md-1 col-sm-1 col-lg-1"><div class="avatar">' + users[msg.user].outerHTML + '</div></div>';
	s += '<div class="col-xs-9 col-md-9 col-sm-9 col-lg-9">' + msg.text + '</div>';
	s += '<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2">' + msg.date + '</div>';
	s += '</div>';
	return s;
}

function newDisconnected(msg) {
	if (!users[msg.user]) users[msg.user] = newAvatar("avatar_" + msg.user, "svg", dim);
	var s = '<div class="row img-rounded disconnected bg-danger">';
	s += '<div class="col-xs-1 col-md-1 col-sm-1 col-lg-1"><div class="avatar">' + users[msg.user].outerHTML + '</div></div>';
	s += '<div class="col-xs-9 col-md-9 col-sm-9 col-lg-9">' + msg.text + '</div>';
	s += '<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2">' + msg.date + '</div>';
	return s;
}

function newAvatar(id, classes, dim) {
	var svg = newSvg(id, classes);
	var color = [getRandomIntInRange(0, 255), getRandomIntInRange(0, 255), getRandomIntInRange(0, 255)];
	for (var i=0; i<dim; i++) {
		for (var j=0; j<dim; j++) {
			if (getRandomInt() === 1) {
				var rect = rectangle((100/dim), i, j, getRandomInt(), color);
				svg.appendChild(rect);
			}
		}
	}
	return svg;
}

function getRandomInt() {
	return Math.round(Math.random());
}

function getRandomIntInRange(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function newSvg(id, classes) {
	var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
	svg.setAttribute('id', id);
	svg.setAttribute('class', classes);
	svg.setAttribute('width', '100%');
	svg.setAttribute('height', '100%');
	svg.setAttribute('viewBox', '0 0 1 1');
	svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
	return svg;
}

function rectangle(dim, i, j, value, color) {
	var newElement = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
	newElement.setAttribute("width", dim + '%');
	newElement.setAttribute("height", dim + '%');
	newElement.setAttribute("x", i*dim + "%");
	newElement.setAttribute("y", j*dim + "%");
	newElement.style.fill = "rgb(" + color[0] + ", " + color[1] + "," + color[2] + ")";
	return newElement;
}