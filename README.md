# README #

## Install

```
npm install
npm install -g nodemon
```

## Execute (dev)

```
nodemon .
```

## Execute (prod)

```
node .
```


## Developing

There are 2 main branches:

* Master (Most stable release) 
* Development (Working release)

In addition to those, there will be individual branches for each Issue to be added/fixed. The flow to follow is:

* Branch from development, put as name the issue number and its title as appears in the issue list
* Start working on the task and making all the commits and push to the branch as desired.
* Once development is finished, create a pull request FROM *<your_branch>* TO *development*, on it, you MUST add the project owner as reviewer, and, if applies, other interested users.
* The reviewers will supervise the code and ask for modifications and improvements, when everything is as expected, the project owner will merge the branch into development and delete the merged branch.